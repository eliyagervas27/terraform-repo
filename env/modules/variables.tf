# Variables
# ----storage_account-----
variable "storage_account_name"  {
  type = string

}

variable "resource_group_name"  {
  type = string
}

# ------resource-group------

variable "resource_group_location"  {
  type = string
}

